# **Despliegue Frontend con Gitlab CI/CD y Cloudformation**
Este es un proyecto que tiene como finalidad automatizar el despliegue de una infraestructura en AWS con la utilización de Cloudformation.

# Cómo usar

**Un nuevo commit en el repositorio disparará el pipeline. Este pipeline tiene tres stages:**

### 1. **CFN Lint**:
Realiza un linter sobre el archivo Cloudformation-stack.yaml, que contiene las configuraciones de la infraestructura a desplegar.

### 2. **Package**:
Construye y pushea a la registry del repositorio (Gitlab) la imagen del contenedor que cumple la función de web server y con el archivo index.html.

### 1. **Cloudformation Deploy**:
Ejecuta una serie de comandos para inciar sesión en AWS y despliega el stack de CloudFormation con el nombre "myStack". Este stage utiliza las variables correspondientes de Gitlab para AWS.

> **_NOTA:_** No debe haber ningún stack con el mismo nombre ya desplegado o en estado de rollback. Todavía no está implementada la lógica para realizar **update** en vez de **deploy**

# Diagrama de Infraestructura

![Diagrama](Diagrama-Infraestructura.png?raw=true)

# Referencias

-   https://about.gitlab.com/blog/2020/12/15/deploy-aws/
-   https://www.udemy.com/course/cloudformation/
-   https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html
-   https://gitlab.com/gitlab-org/cloud-deploy/-/tree/master/aws
-   https://aws.amazon.com/blogs/mt/git-pre-commit-validation-of-aws-cloudformation-templates-with-cfn-lint/
-   https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

# Estado del proyecto
En desarrollo.
